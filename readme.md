[![pipeline status](https://gitlab.com/grola/http-basic-auth/badges/master/pipeline.svg)](https://gitlab.com/grola/http-basic-auth/commits/master)
[![coverage report](https://gitlab.com/grola/http-basic-auth/badges/master/coverage.svg)](https://gitlab.com/grola/http-basic-auth/commits/master)

HTTP Basic Auth for Wordpress. Download or install this plugin from Wordpress Repository: [Basic Auth](https://wordpress.org/plugins/http-basic-auth/).  

**Description**

With this plugin you can protect your Wordpress installation with Basic Auth. Basic Auth can use custom password or Wordpress users login data.

**Installation**

You can install this plugin like any other WordPress plugin.

1. Download and unzip the latest release zip file.
2. Upload the entire plugin directory to your /wp-content/plugins/ directory.
3. Activate the plugin through the Plugins menu in WordPress Administration.

You can also use WordPress uploader to upload plugin zip file in menu Plugins -> Add New -> Upload Plugin. Then go directly to point 3.

