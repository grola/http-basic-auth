<?php
/**
 * Created by PhpStorm.
 * User: gro
 * Date: 06.12.2017
 * Time: 20:50
 */


class test_setup extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
		$this->setup_woocommerce();
		wp_set_current_user(1);
	}

	public function tearDown() {
		parent::tearDown();
	}

	public function setup_woocommerce() {
		update_option( 'woocommerce_calc_taxes', 'yes' );
		update_option( 'woocommerce_tax_based_on', 'billing' );
		update_option( 'woocommerce_default_country', 'PL' );
		update_option( 'woocommerce_prices_include_tax', 'no' );

		//$customer_location = WC_Tax::get_tax_location();

		$tax_rate          = array(
			'tax_rate_country'  => 'PL',
			'tax_rate_state'    => '',
			'tax_rate'          => '23.0000',
			'tax_rate_name'     => 'VAT',
			'tax_rate_priority' => '1',
			'tax_rate_compound' => '0',
			'tax_rate_shipping' => '1',
			'tax_rate_order'    => '1',
			'tax_rate_class'    => '',
		);

		WC_Tax::_insert_tax_rate( $tax_rate );
	}

	public function test_foo_is_foo() {
        $this->assertTrue( 'foo' === 'foo' );
    }

	public function test_is_wc() {
		$this->assertTrue( function_exists( 'WC' ) );
		echo 'WC_VERSION:' . WC_VERSION;
		echo "\n";
	}

	public function test_plugin_version() {
		$plugin_data = get_plugin_data(PLUGIN_PATH . '/http-basic-auth.php' );
		$this->assertEquals( HTTP_BASIC_AUTH_VERSION, $plugin_data['Version'] );
	}

	public function test_plugin_links() {
		$compare_links = array(
			0 => '<a href="' . admin_url( 'options-general.php?page=http-basic-auth-settings' ) . '">Settings</a>',
			1 => '<a href="https://wordpress.org/plugins/basic-auth/">Docs</a>',
			2 => '<a href="https://wordpress.org/support/plugin/basic-auth">Support</a>'
		);
		$http_basic_auth_plugin = http_basic_auth_plugin();
		$links = $http_basic_auth_plugin->links_filter(array());
		$this->assertEquals( $compare_links[0], $links[0] );
		$this->assertEquals( $compare_links[1], $links[1] );
		$this->assertEquals( $compare_links[2], $links[2] );
	}

}
