<?php
/**
 * Created by PhpStorm.
 * User: gro
 * Date: 2018-03-04
 * Time: 18:48
 */

class test_basic_auth extends WP_UnitTestCase {

	/**
	 * @var HTTP_Basic_Auth_Plugin
	 */
	private $basic_auth_plugin = null;

	public function setUP() {
		parent::setUp();
		$this->basic_auth_plugin = http_basic_auth_plugin();
		$settings = $this->basic_auth_plugin->get_settings();
		$settings->update_option( 'enable_basic_auth', 1 );
		$settings->update_option( 'login', 'test' );
		$settings->update_option( 'password', 'testpass' );
	}

	public function test_init_function() {
		$this->assertTrue( function_exists( 'http_basic_auth_plugin' ) );
		$this->assertInstanceOf( 'HTTP_Basic_Auth_Plugin', http_basic_auth_plugin() );
	}

	public function test_class() {
		$this->assertEquals( 'http-basic-auth', $this->basic_auth_plugin->get_namespace() );
		$this->assertEquals( 'http-basic-auth', $this->basic_auth_plugin->get_text_domain() );
	}

	public function test_settings() {
		$this->assertEquals( 1, $this->basic_auth_plugin->get_option( 'enable_basic_auth', 0 ) );
		$this->assertEquals( 'test', $this->basic_auth_plugin->get_option( 'login', '' ) );
		$this->assertEquals( 'testpass', $this->basic_auth_plugin->get_option( 'password', '' ) );
	}

	public function test_plugin_is_active() {
		$this->assertTrue( $this->basic_auth_plugin->plugin_is_active() );
	}

	public function test_is_authenticated_custom() {
		$settings = $this->basic_auth_plugin->get_settings();
		$_SERVER['PHP_AUTH_USER'] = 'c';
		$_SERVER['PHP_AUTH_PW'] = 'c';

		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_custom( 'a', 'b' ) );
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_custom( $settings->get_option( 'login' ), $settings->get_option( 'password' ) ) );

		$_SERVER['PHP_AUTH_USER'] = $settings->get_option( 'login' );
		$_SERVER['PHP_AUTH_PW'] = 'c';
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_custom( $settings->get_option( 'login' ), $settings->get_option( 'password' ) ) );

		$_SERVER['PHP_AUTH_USER'] = 'c';
		$_SERVER['PHP_AUTH_PW'] = $settings->get_option( 'password' );
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_custom( $settings->get_option( 'login' ), $settings->get_option( 'password' ) ) );

		$_SERVER['PHP_AUTH_USER'] = $settings->get_option( 'login' );
		$_SERVER['PHP_AUTH_PW'] = $settings->get_option( 'password' );
		$this->assertTrue( $this->basic_auth_plugin->is_authenticated_custom( $settings->get_option( 'login' ), $settings->get_option( 'password' ) ) );
	}

	public function test_is_authenticates_wordpress_user() {
		wp_create_user( 'wptestuser', 'wptestuserpassword' );

		$_SERVER['PHP_AUTH_USER'] = 'c';
		$_SERVER['PHP_AUTH_PW'] = 'c';
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_wordpress_user() );

		$_SERVER['PHP_AUTH_USER'] = 'wptestuser';
		$_SERVER['PHP_AUTH_PW'] = 'c';
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_wordpress_user() );

		$_SERVER['PHP_AUTH_USER'] = 'c';
		$_SERVER['PHP_AUTH_PW'] = 'wptestuserpassword';
		$this->assertFalse( $this->basic_auth_plugin->is_authenticated_wordpress_user() );

		$_SERVER['PHP_AUTH_USER'] = 'wptestuser';
		$_SERVER['PHP_AUTH_PW'] = 'wptestuserpassword';
		$this->assertTrue( $this->basic_auth_plugin->is_authenticated_wordpress_user() );
	}

}
