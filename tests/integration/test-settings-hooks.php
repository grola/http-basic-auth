<?php
/**
 * Created by PhpStorm.
 * User: gro
 * Date: 2018-03-04
 * Time: 18:48
 */

class test_settings_hooks extends WP_UnitTestCase {

	private $inited_dependencies = false;

	/**
	 * @var HTTP_Basic_Auth_Plugin
	 */
	private $basic_auth_plugin = null;

	/**
	 * @var HTTP_Basic_Auth_Settings_Hooks $settings_hooks
	 */
	private $settings_hooks = null;

	public function setUP() {
		parent::setUp();
		$this->basic_auth_plugin = http_basic_auth_plugin();
		$this->init_dependencies();
		$this->settings_hooks = $this->basic_auth_plugin->get_settings_hooks();
		$settings = $this->basic_auth_plugin->get_settings();
		$settings->update_option( 'enable_basic_auth', 1 );
		$settings->update_option( 'custom_login', 1 );
		$settings->update_option( 'wordpress_login', 1 );
		$settings->update_option( 'login', 'test' );
		$settings->update_option( 'password', 'testpass' );
	}

	private function init_dependencies() {
		if ( !$this->inited_dependencies ) {
			$this->basic_auth_plugin->init_dependencies();
			$this->inited_dependencies = true;
		}
	}

	public function remove_filters() {
		$func = str_replace( '-', '_', $this->basic_auth_plugin->get_namespace() );

		// settings menu
		remove_filter( $func . '_menu', array( $this->settings_hooks, 'settings_menu' ) );

		// settings tabs
		remove_filter( $func . '_settings_tabs', array( $this->settings_hooks, 'settings_tabs' ) );

		// unsavable tabs
		remove_filter( $func . '_unsavable_tabs', array( $this->settings_hooks, 'unsavable_tabs' ) );

		// settings sections
		remove_filter( $func . '_registered_settings_sections', array( $this->settings_hooks, 'registered_settings_sections' ) );

		// settings
		remove_filter( $func . '_registered_settings', array( $this->settings_hooks, 'registered_settings' ) );

		// custom type hook
		remove_action( $func . '_thanks', array( $this->settings_hooks, 'custom_type_thanks' ) );

		remove_action( $func . '_settings_tab_bottom_settings_options', array( $this->settings_hooks, 'settings_tab_bottom_settings_options' ) );

	}

	public function test_hooks() {
		global $wp_filter;
		$this->remove_filters();
		$all_hooks = count( $wp_filter );
		$this->settings_hooks->hooks();
		$this->assertEquals( $all_hooks + 7, count( $wp_filter ) );
	}

	public function test_settings_menu() {
		$this->init_dependencies();
		$settings_menu = $this->settings_hooks->settings_menu( array() );
		$this->assertEquals( 8, count( $settings_menu ) );
	}

	public function test_settings_tabs() {
		$this->init_dependencies();
		$settings_tabs = $this->settings_hooks->settings_tabs( array() );
		$this->assertEquals( 3, count( $settings_tabs ) );
	}

	public function test_unsavable_tabs() {
		$this->init_dependencies();
		$unsavable_tabs = $this->settings_hooks->unsavable_tabs( array() );
		$this->assertEquals( 1, count( $unsavable_tabs ) );
	}

	public function test_registered_settings_sections() {
		$this->init_dependencies();
		$registered_settings_sections = $this->settings_hooks->registered_settings_sections( array() );
		$this->assertEquals( 3, count( $registered_settings_sections ) );
	}

	public function test_registered_settings() {
		$this->init_dependencies();
		$registered_settings = $this->settings_hooks->registered_settings( array() );
		$this->assertEquals( 3, count( $registered_settings ) );
		$this->assertEquals( 1, count( $registered_settings['settings'] ) );
		$this->assertEquals( 16, count( $registered_settings['settings']['options'] ) );
	}

}